# Seefood


### A BRIEF NOTE ON HOW TO USE GPU TRAINER:

Step 1. Install CUDA/TensorFlow prerequisites according to: 

https://docs.microsoft.com/en-us/dotnet/api/microsoft.ml.vision.imageclassificationtrainer?view=ml-dotnet

Step 2. Modify the configuration parameters in "ModelBuilder.cs" in the "/mlnet_image_classification_gpu_trainer/stanford dogs/stanford dogsML.ConsoleApp/" directory

Step 3. This package is preconfigured to use GPU TensorFlow for Windows. You should be able to run it now. The app will generate a .zip model in the path you specified in the configuration.





### IDEAL "CLEAN SOLUTION" WORKFLOW PROCEDURE:


Step 1.  Terminate all running instances of the program

Step 2.  Build Menu -> "Rebuild Solution"

Step 3.  Build Menu -> "Clean Solution"

Step 4.  Add, commit, and push.


##### This is now optional thanks to .gitignore
