﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MetaGenerator
{
    public partial class Form1 : Form
    {
        string DEFAULT_PATTERN = "\t";

        string DEFAULT_REGEX_REPLACEMENT_PATTERN = @"[\t].+?[-]";


        public Form1()
        {
            InitializeComponent();
            txtRegexPattern.Text = DEFAULT_REGEX_REPLACEMENT_PATTERN;
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            if (ofdOpenFile.ShowDialog() == DialogResult.OK)
            {
                string[] lines = File.ReadAllLines(ofdOpenFile.FileName);
                
                 txtOutput.Text = "            [EnumModels.REPLACE_THIS_WITH_NEW_KEY] = new Dictionary<int, string>()" 
                    + Environment.NewLine + "            {" + Environment.NewLine;

                string fixedLine = "";
                for (int i = 1; i < lines.Length; ++i)
                {

                    fixedLine = "                [" + lines[i].Replace("\r", "");

                    if (chkUseRegex.Checked)
                        fixedLine = Regex.Replace(fixedLine, txtRegexPattern.Text, "] = \"") + "\",";
                    else
                        fixedLine = fixedLine.Replace("\t", "] = \"") + "\",";
    
                    txtOutput.Text += fixedLine + Environment.NewLine;
                }

                txtOutput.Text += "            }," + Environment.NewLine + Environment.NewLine;

                txtOutput.SelectAll();
                txtOutput.Focus();
            }
        }

        private void ChkUseRegex_CheckedChanged(object sender, EventArgs e) => txtRegexPattern.Enabled = chkUseRegex.Checked;
    }
}
