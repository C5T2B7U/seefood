﻿namespace Seefood
{
    partial class FormUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormUI));
            this.ofdOpenFile = new System.Windows.Forms.OpenFileDialog();
            this.pbxImage = new System.Windows.Forms.PictureBox();
            this.btnSelectImage = new System.Windows.Forms.Button();
            this.txtCredits = new System.Windows.Forms.TextBox();
            this.tabCtrlMenu = new System.Windows.Forms.TabControl();
            this.tabAbout = new System.Windows.Forms.TabPage();
            this.tabImage = new System.Windows.Forms.TabPage();
            this.gbClassify = new System.Windows.Forms.GroupBox();
            this.dropdownModelSelection = new System.Windows.Forms.ComboBox();
            this.lblChooseModel = new System.Windows.Forms.Label();
            this.gbImageContainer = new System.Windows.Forms.GroupBox();
            this.lblNoImageLoaded = new System.Windows.Forms.Label();
            this.tabStatistics = new System.Windows.Forms.TabPage();
            this.txtStatistics = new System.Windows.Forms.TextBox();
            this.btnAbortValidation = new System.Windows.Forms.Button();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.pnlContainer = new System.Windows.Forms.Panel();
            this.txtStatus = new System.Windows.Forms.TextBox();
            this.workerThread = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.pbxImage)).BeginInit();
            this.tabCtrlMenu.SuspendLayout();
            this.tabAbout.SuspendLayout();
            this.tabImage.SuspendLayout();
            this.gbClassify.SuspendLayout();
            this.gbImageContainer.SuspendLayout();
            this.tabStatistics.SuspendLayout();
            this.pnlContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // ofdOpenFile
            // 
            this.ofdOpenFile.FilterIndex = 0;
            // 
            // pbxImage
            // 
            this.pbxImage.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pbxImage.Location = new System.Drawing.Point(6, 12);
            this.pbxImage.Name = "pbxImage";
            this.pbxImage.Size = new System.Drawing.Size(745, 373);
            this.pbxImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbxImage.TabIndex = 0;
            this.pbxImage.TabStop = false;
            // 
            // btnSelectImage
            // 
            this.btnSelectImage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectImage.BackColor = System.Drawing.Color.LightBlue;
            this.btnSelectImage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSelectImage.Font = new System.Drawing.Font("Verdana", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectImage.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnSelectImage.Location = new System.Drawing.Point(6, 53);
            this.btnSelectImage.Name = "btnSelectImage";
            this.btnSelectImage.Size = new System.Drawing.Size(659, 33);
            this.btnSelectImage.TabIndex = 1;
            this.btnSelectImage.Text = "Click here to select an image and perform classification!";
            this.btnSelectImage.UseVisualStyleBackColor = false;
            this.btnSelectImage.Click += new System.EventHandler(this.BtnSelectImage_Click);
            // 
            // txtCredits
            // 
            this.txtCredits.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCredits.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtCredits.Font = new System.Drawing.Font("Verdana", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCredits.ForeColor = System.Drawing.Color.MidnightBlue;
            this.txtCredits.Location = new System.Drawing.Point(0, 3);
            this.txtCredits.Multiline = true;
            this.txtCredits.Name = "txtCredits";
            this.txtCredits.ReadOnly = true;
            this.txtCredits.Size = new System.Drawing.Size(757, 474);
            this.txtCredits.TabIndex = 5;
            this.txtCredits.Text = resources.GetString("txtCredits.Text");
            this.txtCredits.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tabCtrlMenu
            // 
            this.tabCtrlMenu.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabCtrlMenu.Controls.Add(this.tabAbout);
            this.tabCtrlMenu.Controls.Add(this.tabImage);
            this.tabCtrlMenu.Controls.Add(this.tabStatistics);
            this.tabCtrlMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabCtrlMenu.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabCtrlMenu.Location = new System.Drawing.Point(0, 0);
            this.tabCtrlMenu.Name = "tabCtrlMenu";
            this.tabCtrlMenu.SelectedIndex = 0;
            this.tabCtrlMenu.Size = new System.Drawing.Size(768, 512);
            this.tabCtrlMenu.TabIndex = 6;
            // 
            // tabAbout
            // 
            this.tabAbout.Controls.Add(this.txtCredits);
            this.tabAbout.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.tabAbout.Location = new System.Drawing.Point(4, 28);
            this.tabAbout.Name = "tabAbout";
            this.tabAbout.Padding = new System.Windows.Forms.Padding(3);
            this.tabAbout.Size = new System.Drawing.Size(760, 480);
            this.tabAbout.TabIndex = 0;
            this.tabAbout.Text = "   About Program   ";
            this.tabAbout.UseVisualStyleBackColor = true;
            // 
            // tabImage
            // 
            this.tabImage.Controls.Add(this.gbClassify);
            this.tabImage.Controls.Add(this.gbImageContainer);
            this.tabImage.Location = new System.Drawing.Point(4, 28);
            this.tabImage.Name = "tabImage";
            this.tabImage.Padding = new System.Windows.Forms.Padding(3);
            this.tabImage.Size = new System.Drawing.Size(760, 480);
            this.tabImage.TabIndex = 1;
            this.tabImage.Text = "   Image Classification   ";
            this.tabImage.UseVisualStyleBackColor = true;
            // 
            // gbClassify
            // 
            this.gbClassify.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbClassify.Controls.Add(this.btnAbortValidation);
            this.gbClassify.Controls.Add(this.dropdownModelSelection);
            this.gbClassify.Controls.Add(this.btnSelectImage);
            this.gbClassify.Controls.Add(this.lblChooseModel);
            this.gbClassify.Location = new System.Drawing.Point(0, 384);
            this.gbClassify.Name = "gbClassify";
            this.gbClassify.Size = new System.Drawing.Size(757, 97);
            this.gbClassify.TabIndex = 11;
            this.gbClassify.TabStop = false;
            // 
            // dropdownModelSelection
            // 
            this.dropdownModelSelection.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dropdownModelSelection.BackColor = System.Drawing.Color.LightPink;
            this.dropdownModelSelection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dropdownModelSelection.Font = new System.Drawing.Font("Verdana", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dropdownModelSelection.ForeColor = System.Drawing.Color.MidnightBlue;
            this.dropdownModelSelection.FormattingEnabled = true;
            this.dropdownModelSelection.Location = new System.Drawing.Point(285, 16);
            this.dropdownModelSelection.Name = "dropdownModelSelection";
            this.dropdownModelSelection.Size = new System.Drawing.Size(466, 31);
            this.dropdownModelSelection.TabIndex = 8;
            this.dropdownModelSelection.SelectedIndexChanged += new System.EventHandler(this.DropdownModelSelection_SelectedIndexChanged);
            // 
            // lblChooseModel
            // 
            this.lblChooseModel.AutoSize = true;
            this.lblChooseModel.Font = new System.Drawing.Font("Verdana", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChooseModel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.lblChooseModel.Location = new System.Drawing.Point(6, 19);
            this.lblChooseModel.Name = "lblChooseModel";
            this.lblChooseModel.Size = new System.Drawing.Size(273, 23);
            this.lblChooseModel.TabIndex = 9;
            this.lblChooseModel.Text = "Choose classifier model:";
            // 
            // gbImageContainer
            // 
            this.gbImageContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbImageContainer.Controls.Add(this.lblNoImageLoaded);
            this.gbImageContainer.Controls.Add(this.pbxImage);
            this.gbImageContainer.Location = new System.Drawing.Point(0, -5);
            this.gbImageContainer.Name = "gbImageContainer";
            this.gbImageContainer.Size = new System.Drawing.Size(757, 390);
            this.gbImageContainer.TabIndex = 12;
            this.gbImageContainer.TabStop = false;
            // 
            // lblNoImageLoaded
            // 
            this.lblNoImageLoaded.AutoSize = true;
            this.lblNoImageLoaded.Font = new System.Drawing.Font("Verdana", 22F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoImageLoaded.ForeColor = System.Drawing.Color.DarkRed;
            this.lblNoImageLoaded.Location = new System.Drawing.Point(217, 169);
            this.lblNoImageLoaded.Name = "lblNoImageLoaded";
            this.lblNoImageLoaded.Size = new System.Drawing.Size(312, 36);
            this.lblNoImageLoaded.TabIndex = 10;
            this.lblNoImageLoaded.Text = "No Image Loaded";
            // 
            // tabStatistics
            // 
            this.tabStatistics.Controls.Add(this.txtStatistics);
            this.tabStatistics.Location = new System.Drawing.Point(4, 28);
            this.tabStatistics.Name = "tabStatistics";
            this.tabStatistics.Size = new System.Drawing.Size(760, 480);
            this.tabStatistics.TabIndex = 2;
            this.tabStatistics.Text = "   Statistics   ";
            this.tabStatistics.UseVisualStyleBackColor = true;
            // 
            // txtStatistics
            // 
            this.txtStatistics.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStatistics.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtStatistics.Font = new System.Drawing.Font("Courier New", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStatistics.Location = new System.Drawing.Point(0, 3);
            this.txtStatistics.Multiline = true;
            this.txtStatistics.Name = "txtStatistics";
            this.txtStatistics.ReadOnly = true;
            this.txtStatistics.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtStatistics.Size = new System.Drawing.Size(757, 474);
            this.txtStatistics.TabIndex = 0;
            this.txtStatistics.Text = "\r\nStatistics not yet generated.";
            // 
            // btnAbortValidation
            // 
            this.btnAbortValidation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAbortValidation.BackColor = System.Drawing.Color.LightBlue;
            this.btnAbortValidation.Enabled = false;
            this.btnAbortValidation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAbortValidation.Font = new System.Drawing.Font("Verdana", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAbortValidation.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnAbortValidation.Location = new System.Drawing.Point(671, 53);
            this.btnAbortValidation.Name = "btnAbortValidation";
            this.btnAbortValidation.Size = new System.Drawing.Size(80, 33);
            this.btnAbortValidation.TabIndex = 3;
            this.btnAbortValidation.Text = "Abort";
            this.btnAbortValidation.UseVisualStyleBackColor = false;
            this.btnAbortValidation.Click += new System.EventHandler(this.BtnAbortValidation_Click);
            // 
            // progressBar
            // 
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar.Location = new System.Drawing.Point(14, 528);
            this.progressBar.MarqueeAnimationSpeed = 25;
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(757, 33);
            this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar.TabIndex = 7;
            // 
            // pnlContainer
            // 
            this.pnlContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlContainer.Controls.Add(this.tabCtrlMenu);
            this.pnlContainer.Location = new System.Drawing.Point(10, 12);
            this.pnlContainer.Name = "pnlContainer";
            this.pnlContainer.Size = new System.Drawing.Size(768, 512);
            this.pnlContainer.TabIndex = 8;
            // 
            // txtStatus
            // 
            this.txtStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtStatus.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.txtStatus.Font = new System.Drawing.Font("Verdana", 14F, System.Drawing.FontStyle.Bold);
            this.txtStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.txtStatus.Location = new System.Drawing.Point(14, 528);
            this.txtStatus.Multiline = true;
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.ReadOnly = true;
            this.txtStatus.Size = new System.Drawing.Size(757, 30);
            this.txtStatus.TabIndex = 9;
            this.txtStatus.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // workerThread
            // 
            this.workerThread.WorkerReportsProgress = true;
            // 
            // FormUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(790, 570);
            this.Controls.Add(this.pnlContainer);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.txtStatus);
            this.MinimumSize = new System.Drawing.Size(800, 600);
            this.Name = "FormUI";
            this.Text = "    Multi-Model Image Classifier   ";
            ((System.ComponentModel.ISupportInitialize)(this.pbxImage)).EndInit();
            this.tabCtrlMenu.ResumeLayout(false);
            this.tabAbout.ResumeLayout(false);
            this.tabAbout.PerformLayout();
            this.tabImage.ResumeLayout(false);
            this.gbClassify.ResumeLayout(false);
            this.gbClassify.PerformLayout();
            this.gbImageContainer.ResumeLayout(false);
            this.gbImageContainer.PerformLayout();
            this.tabStatistics.ResumeLayout(false);
            this.tabStatistics.PerformLayout();
            this.pnlContainer.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog ofdOpenFile;
        private System.Windows.Forms.PictureBox pbxImage;
        private System.Windows.Forms.Button btnSelectImage;
        private System.Windows.Forms.TextBox txtCredits;
        private System.Windows.Forms.TabControl tabCtrlMenu;
        private System.Windows.Forms.TabPage tabAbout;
        private System.Windows.Forms.TabPage tabImage;
        private System.Windows.Forms.TabPage tabStatistics;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Panel pnlContainer;
        private System.Windows.Forms.ComboBox dropdownModelSelection;
        private System.Windows.Forms.Label lblChooseModel;
        private System.Windows.Forms.TextBox txtStatus;
        private System.ComponentModel.BackgroundWorker workerThread;
        private System.Windows.Forms.TextBox txtStatistics;
        private System.Windows.Forms.Label lblNoImageLoaded;
        private System.Windows.Forms.GroupBox gbClassify;
        private System.Windows.Forms.GroupBox gbImageContainer;
        private System.Windows.Forms.Button btnAbortValidation;
    }
}
