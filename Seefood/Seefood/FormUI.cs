﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using SeefoodML.ConsoleApp;
using SeefoodML.Model;


namespace Seefood
{
    public partial class FormUI : Form
    {

        #region Declarations


        // Set the maximum number of predictions to validate per class
        //private int MAX_VALIDATION_CLASS_SIZE = 100;
        //private int MAX_VALIDATION_CLASS_SIZE = 2;
        //private int MAX_VALIDATION_CLASS_SIZE = 1000;
        //private int MAX_VALIDATION_CLASS_SIZE = 24000;
        private int MAX_VALIDATION_CLASS_SIZE = 100000;


        // Set the default prediction model for the dropdown box
        private const int DEFAULT_MODEL_INDEX = 0;


        // Set default model filename
        string DEFAULT_MODEL_FILENAME = Meta.ModelFilenames[(Meta.EnumModels) DEFAULT_MODEL_INDEX];


        // Set the allowable filetypes
        private string ALLOWABLE_FILETYPES_FILTER =
            "Applicable Images|*.jpg;*.jpeg;*.png|" +
            "Model Validation TSV|*.tsv|" +
            "Use Per-Class Limit|*.LIMIT_*.tsv";


        // States
        private bool isInterfaceLocked = false;
        private bool isWorking = false;
        private bool doAbort = false;
        private bool allowAbort = false;


        // Variables to set in initialization
        private string modelFilename;
        private int modelIndex;
        private int validationLimit;

        #endregion


        #region Initialization


        // Form constructor
        public FormUI()
        {
            // Always initialize the form
            InitializeComponent();

            // Configure the OpenFileDialog control
            ofdOpenFile.Filter = ALLOWABLE_FILETYPES_FILTER;

            // Configure the BackgroundWorker thread
            workerThread.DoWork += new DoWorkEventHandler(WorkerThread_DoWork);
            workerThread.ProgressChanged += new ProgressChangedEventHandler(WorkerThread_ProgressChanged);

            // Initialize the rest of the program
            InitList();
            InitDefaults();
            RefreshForm();
        }

        // Initialize any lists
        private void InitList()
        {
            // Get all the names of the models available
            dropdownModelSelection.DataSource = Meta.ModelTitles.Values.ToList<string>();

            // TODO maybe:  Remove items whose model files were not found
        }

        // Initialize any uninitialized default values
        private void InitDefaults()
        {
            modelFilename = DEFAULT_MODEL_FILENAME;
            modelIndex = DEFAULT_MODEL_INDEX;
            dropdownModelSelection.SelectedIndex = DEFAULT_MODEL_INDEX;
            validationLimit = MAX_VALIDATION_CLASS_SIZE;
        }


        #endregion


        #region Form / Data Interface


        // This disciplines the form so it doesn't act up
        private void RefreshForm()
        {
            // Lock the button?
            btnSelectImage.FlatStyle = (isInterfaceLocked ? FlatStyle.System : FlatStyle.Flat);
            btnSelectImage.Enabled = !isInterfaceLocked;
            btnAbortValidation.Enabled = allowAbort && !doAbort;
            btnAbortValidation.FlatStyle = (btnAbortValidation.Enabled ? FlatStyle.Flat : FlatStyle.System);

            // Lock the dropdown?
            dropdownModelSelection.Enabled = !isInterfaceLocked;

            // Swap the status controls?
            //txtStatus.Visible = !isWorking;
            progressBar.Style = ( isWorking && !allowAbort ? ProgressBarStyle.Marquee : ProgressBarStyle.Continuous );
        }


        // This tries to run the model prediction
        private void TryRunPrediction()
        {
            try
            {
                // Run the prediction if user selects a file
                if (ofdOpenFile.ShowDialog() == DialogResult.OK)
                {
                    // Lock the UI
                    isInterfaceLocked = true;
                    lblNoImageLoaded.Visible = false;
                    RefreshForm();

                    if (!ofdOpenFile.FileName.EndsWith(".tsv"))
                    {
                        // Update the PictureBox control
                        Image image = Image.FromFile(ofdOpenFile.FileName);
                        pbxImage.Image = image;
                    }
                    else
                    {
                        allowAbort = true;
                    }

                    // Release the hounds!
                    workerThread.RunWorkerAsync();

                }
            }
            catch (Exception ex)
            {
                // This shouldn't happen
                MessageBox.Show(ex.Message);
                isInterfaceLocked = false;
            }
            finally
            {
                RefreshForm();
            }
        }


        // This generates the output text from the prediction results
        private void GenerateResults(ModelOutput result)
        {
            try
            {
                // Local temp variables
                var scoreList = result.Score.ToList<float>();
                var sortedScores = new Dictionary<int, float>();
                TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                int maxIndex = -1;
                int rank = 0;
                string predictionClass = "";
                float maxScore = -1;
                int predictionIndex = -1;
                string prettyPrediction = "";

                // Generate the stats bro
                string stats = Environment.NewLine +
                    "Using model:  " + Meta.ModelTitles[(Meta.EnumModels)modelIndex] +
                    Environment.NewLine +
                    Environment.NewLine +
                    "Model file:   " + Meta.ModelFilenames[(Meta.EnumModels)modelIndex] +
                    Environment.NewLine +
                    Environment.NewLine +
                    "Input image:    " + ofdOpenFile.FileName + 
                    Environment.NewLine +
                    Environment.NewLine +
                    Environment.NewLine;


                // We just need to sort the scores but keep the index where they were originally found, so the labels can be applied
                foreach (var placeholder in result.Score)
                {
                    maxScore = scoreList.Max();
                    maxIndex = Array.IndexOf(result.Score, maxScore);
                    sortedScores[maxIndex] = maxScore;
                    scoreList.RemoveAt(scoreList.IndexOf(maxScore));
                    if (predictionIndex < 0)
                    {
                        predictionIndex = maxIndex;
                        prettyPrediction = textInfo.ToTitleCase(Meta.PredictionClasses[(Meta.EnumModels)modelIndex][maxIndex].Replace("_", " "));
                    }
                }
                foreach (KeyValuePair<int, float> score in sortedScores)
                {
                    ++rank;
                    predictionClass = textInfo.ToTitleCase(Meta.PredictionClasses[(Meta.EnumModels) modelIndex][score.Key].Replace("_", " "));
                    stats += string.Format("{0,3}\t {1,6:0.00}%    \t{2,0}", rank, (score.Value * 100), predictionClass) +
                        Environment.NewLine;
                }
                txtStatistics.Text = stats;
                txtStatistics.Refresh();


                // Display the correct output based on prediction categories defined in the model
                if (result.Prediction == "hotdog" || result.Prediction == "hot_dog")
                    txtStatus.Text = "This is " + Math.Round(result.Score.Max() * 100, 2) + "% likely to be a hotdog.";

                else if (result.Prediction == "not_hotdog" || result.Prediction == "not_hot_dog")
                    txtStatus.Text = "This is " + Math.Round(result.Score.Max() * 100, 2) + "% likely to not be a hotdog.";

                else
                    txtStatus.Text = "This is " + Math.Round(result.Score.Max() * 100, 2) + "% likely to be:    " + prettyPrediction;
                
                txtStatus.Visible = true;
                txtStatus.Refresh();




            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                RefreshForm();
            }
        }


        private void GenerateValidationResults(Dictionary<string, List<KeyValuePair<string, float>>> resultsDictionary)
        {
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
            string stats = "";
            int index = 0;

            int numClasses = resultsDictionary.Count;
            int totalPredictions = 0;
            
            int totalMatches = 0;
            int matchesPerClass = 0;
            
            float totalScoreSum = 0;
            float scoreSumPerClass = 0;

            double totalStdDevPerClass = 0;
            double stdDevPerClass = 0;

            int classSize = 0;

            txtStatus.Text = "Generating results.  Please wait...";
            txtStatus.Refresh();


            foreach (var expectedClass in resultsDictionary)
            {
                var scoreList = new List<double>();

                stats += string.Format("{1,0}{1,0}{1,0}Expected class:  {0,0} {1,0}{1,0}", expectedClass.Key, Environment.NewLine);

                classSize = expectedClass.Value.Count;
                matchesPerClass = 0;
                scoreSumPerClass = 0;
                bool isMatch = false;
                index = 0;

                foreach (var prediction in expectedClass.Value)
                {
                    isMatch = expectedClass.Key.ToUpper() == prediction.Key.ToUpper();
                    matchesPerClass += (isMatch ? 1 : 0);
                    
                    stats += string.Format("\tImage:  {0,0}    Match:  {1,0}    Score:  {2,6:0.00}%  {3,0}{4,0}",
                        index++,
                        (isMatch ? "Yes" : "No "),
                        prediction.Value,
                        (isMatch ? "" : "=>  " + string.Format("{0,5:0.00}%", (numClasses == 2 ? (100.0 - prediction.Value) : 0)) + Environment.NewLine + "\t\tPrediction:  " + prediction.Key),
                        Environment.NewLine);

                    if (isMatch)
                    {
                        scoreList.Add(prediction.Value);
                        scoreSumPerClass += prediction.Value;
                    }
                    else
                    {
                        scoreList.Add((numClasses == 2 ? (100.0 - prediction.Value) : 0));
                        scoreSumPerClass += (float)(numClasses == 2 ? (100.0 - prediction.Value) : 0);
                    }
                }

                // Generate subtotals per prediction class
                stdDevPerClass = ModelBuilder.CalculateStandardDeviation(scoreList);
                if (double.IsNaN(stdDevPerClass))
                    stdDevPerClass = 0;

                totalStdDevPerClass += stdDevPerClass;

                totalMatches += matchesPerClass;
                totalScoreSum += scoreSumPerClass;
                totalPredictions += classSize;

                stats += string.Format("{2,0}{2,0}Matches: \t\t\t {0,0}  of:  {1,0}{2,0}",
                    matchesPerClass, classSize, Environment.NewLine);

                stats += string.Format("Percentage matches: \t\t{0,6:0.00}%{1,0}",
                    (float)matchesPerClass / (float)classSize * 100.0f, Environment.NewLine);

                stats += string.Format("Average score per class: \t{0,6:0.00}%{1,0}",
                    scoreList.Average(), Environment.NewLine);

                stats += string.Format("Standard deviation:      \t{0,6:0.00}%{1,0}",
                    stdDevPerClass, Environment.NewLine);
            }


            // Generate totals
            stats = string.Format(
                "{0,0}Model Batch Validation{0,0}{0,0}{0,0}{0,0}{0,0}" +
                "Using model:   {1,0}{0,0}{0,0}" +
                "Model file:        \"{2,0}\"{0,0}{0,0}" +
                "Validation file:   \"{3,0}\"{0,0}{0,0}" +
                "Number of classes:  {4,0}{0,0}{0,0}" +
                "Maximum predictions per class:  {5,0}{0,0}{0,0}" +
                "{0,0}{0,0}{0,0}",
                    Environment.NewLine,
                    Meta.ModelTitles[(Meta.EnumModels)modelIndex],
                    Meta.ModelFilenames[(Meta.EnumModels)modelIndex],
                    ofdOpenFile.FileName,
                    numClasses,
                    validationLimit
                ) +
                
                string.Format("Matches: \t\t\t {0,0}  of:  {1,0}{2,0}",
                    totalMatches, totalPredictions, Environment.NewLine) + 

                string.Format("Percentage matches: \t\t{0,6:0.00}%{1,0}",
                    (float)totalMatches / (float)totalPredictions * 100.0f, Environment.NewLine) + 

                string.Format("Average score per class: \t{0,6:0.00}%{1,0}",
                    totalScoreSum / (float)totalPredictions, Environment.NewLine) +

                string.Format("Average standard deviation:\t{0,6:0.00}%{1,0}",
                    (float)totalStdDevPerClass / (float)resultsDictionary.Keys.Count, Environment.NewLine) +
            
                stats;


            txtStatistics.Text = stats;
            txtStatistics.Refresh();
            txtStatistics.Select(0, 0);
            txtStatistics.ScrollToCaret();

            txtStatus.Text = "Model validation statistics have been generated.";
            txtStatus.Refresh();
            tabCtrlMenu.SelectedIndex = 2;
        }


        #endregion


        #region Asynchronous Methods


        // Asynchronously run the image through the classifier on the worker thread
        private void AsyncPredictImage(string filePath)
        {
            try
            {
                // Report 1% progress
                workerThread.ReportProgress(1);

                if (!filePath.EndsWith(".tsv"))
                {
                    // Create an input using the specific model that was imported by the directive
                    var input = new ModelInput
                    {
                        ImageSource = filePath
                    };

                    // Load model using custom filename and predict output of sample data
                    ModelOutput result = ConsumeModel.Predict(input, modelFilename);

                    // Report 100% progress and send back the prediction result
                    workerThread.ReportProgress(100, result);
                }
                else
                {
                    validationLimit = MAX_VALIDATION_CLASS_SIZE;
                    foreach (string filePathSegment in filePath.Split("."))
                    {
                        if (filePathSegment.ToUpper().StartsWith("LIMIT_"))
                        {
                            var segmentSplit = filePathSegment.Split("_");
                            if (segmentSplit.Length == 2)
                            {
                                // If an int comes after "LIMIT_", that's the validation limit
                                if (int.TryParse(segmentSplit[1], out int newValidationLimit))
                                    validationLimit = newValidationLimit;
                                // If it was an int, great. If not, oh well try again.
                            }
                        }
                    }

                    Dictionary<string, List<KeyValuePair<string, float>>> resultsDictionary = new Dictionary<string, List<KeyValuePair<string, float>>>();

                    string imagePath;
                    string expectedClass;
                    string predictedClass;

                    IList<string> lines = System.IO.File.ReadAllLines(filePath).ToList<string>();
                    workerThread.ReportProgress(45, lines.Count - 1);

                    for (int index = 1; index < lines.Count; ++index)
                    {
                        imagePath = lines[index].Split("\t")[1];
                        expectedClass = lines[index].Split("\t")[0];

                        if (!resultsDictionary.ContainsKey(expectedClass))
                            resultsDictionary.Add(expectedClass, new List<KeyValuePair<string, float>>());
                        
                        if (resultsDictionary[expectedClass].Count < validationLimit)
                        {

                            workerThread.ReportProgress(50, imagePath);
                            workerThread.ReportProgress(55, index - 1);

                            var input = new ModelInput
                            {
                                ImageSource = imagePath
                            };

                            // Load model using custom filename and predict output of sample data
                            ModelOutput result = ConsumeModel.Predict(input, modelFilename);
                            predictedClass = result.Prediction;

                            resultsDictionary[expectedClass].Add(new KeyValuePair<string, float>(result.Prediction, result.Score.Max() * 100));
                        }

                        if (doAbort)
                            break;
                    }

                    workerThread.ReportProgress(99, resultsDictionary);
                }
            }
            catch (Exception ex)
            {
                workerThread.ReportProgress(0, ex);
            }
        }


        #endregion


        #region Event Handlers


        // This handles the button click
        private void BtnSelectImage_Click(object sender, EventArgs e) => TryRunPrediction();

        private void BtnAbortValidation_Click(object sender, EventArgs e)
        {
            doAbort = true;
            txtStatus.Text = "Aborting.  Please wait...";
            progressBar.Visible = false;
            RefreshForm();
        }


        // This will ensure the user can select the model through the dropdown
        private void DropdownModelSelection_SelectedIndexChanged(object sender, EventArgs e)
        {
            int selectedIndex = dropdownModelSelection.SelectedIndex;

            modelIndex = selectedIndex;
            modelFilename = Meta.ModelFilenames[(Meta.EnumModels) selectedIndex];

            progressBar.Visible = true;

            if (!lblNoImageLoaded.Visible)
                txtStatistics.Text = Environment.NewLine + "Selected classifier model changed.  Please perform image classification again.";

        }


        // Who let the dogs out?  This, this guy did
        void WorkerThread_DoWork(object sender, DoWorkEventArgs e) => AsyncPredictImage(ofdOpenFile.FileName);


        // Scrum bro, let's have a standup
        void WorkerThread_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                switch (e.ProgressPercentage)
                {
                    case 0:
                        // Whoops, worker caught an exception
                        throw new Exception(((Exception)e.UserState).Message);
                        //break;

                    case 1:
                        // Prediction started
                        isWorking = true;
                        progressBar.Visible = true;
                        txtStatistics.Text = string.Format(
                                                "{0,0}Now generating model validation....{0,0}{0,0}{0,0}{0,0}{0,0}" +
                                                "Using model:   {1,0}{0,0}{0,0}" +
                                                "Model file:        \"{2,0}\"{0,0}{0,0}" +
                                                "Validation file:   \"{3,0}\"{0,0}{0,0}" +
                                                "Maximum predictions per class:  {4,0}{0,0}{0,0}" +
                                                "{0,0}{0,0}{0,0}",
                                                    Environment.NewLine,
                                                    Meta.ModelTitles[(Meta.EnumModels)modelIndex],
                                                    Meta.ModelFilenames[(Meta.EnumModels)modelIndex],
                                                    ofdOpenFile.FileName,
                                                    validationLimit
                                                );
                        break;

                    case 45:
                        progressBar.Maximum = (int)e.UserState;
                        break;

                    case 50:
                        pbxImage.Image = Image.FromFile((string)e.UserState);
                        pbxImage.Refresh();
                        break;

                    case 55:
                        progressBar.Value = (int)e.UserState;
                        break;

                    case 99:
                        progressBar.Style = ProgressBarStyle.Marquee;
                        GenerateValidationResults((Dictionary<string, List<KeyValuePair<string, float>>>)e.UserState);
                        isWorking = false;
                        isInterfaceLocked = false;
                        progressBar.Visible = false;
                        doAbort = false;
                        allowAbort = false;
                        break;


                    case 100:
                        // Prediction finished, so cast userstate to ModelOutput and generate results
                        GenerateResults((ModelOutput)e.UserState);
                        isWorking = false;
                        isInterfaceLocked = false;
                        progressBar.Visible = false;
                        break;

                    default:
                        // This won't happen.  So don't let it, ok
                        break;
                }
            }
            catch (Exception ex)
            {
                // Whoops, exception
                MessageBox.Show(ex.GetType() + Environment.NewLine + ex.Message);
                isWorking = false;
                isInterfaceLocked = false;
                doAbort = false;
                allowAbort = false;

                txtStatistics.Text = Environment.NewLine + "Encountered error.";
            }
            finally
            {
                RefreshForm();
            }
        }


        #endregion

    }
}