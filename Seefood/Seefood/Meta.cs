﻿using System.Collections.Generic;

namespace Seefood
{
    internal static class Meta
    {
        private static string TITLE_HEADER = "   ";


        // The enumeration of identifiers representing each model available to the program.
        // NOTE:  The order of the enumeration must match the order of the titles dictionary.
        internal enum EnumModels : int
        {
            StanfordDogs = 0,
            CatsVsDogs,
            Vehicles,
            Mammals,
            Sports,
            HorseOrHuman,
            Food101,
            HotdogNotHotdogFood101,

        }


        // The dictionary of display text for each model, to be shown in the dropdown.
        // NOTE:  The order of these dictionary definitions controls the placement in the dropdown.
        // NOTE:  The order of the enumeration must match the order of the titles dictionary.
        internal static IDictionary<EnumModels, string> ModelTitles = new Dictionary<EnumModels, string>()
        {
            [EnumModels.StanfordDogs] = TITLE_HEADER +              "Stanford Dogs                 [Official]",
            [EnumModels.CatsVsDogs] = TITLE_HEADER +                "Cats vs. Dogs                  [Official]",
            [EnumModels.Vehicles] = TITLE_HEADER +                  "Vehicles                          [Ben LeMarc]",
            [EnumModels.Mammals] = TITLE_HEADER +                 "Mammals                        [Ben LeMarc]",
            [EnumModels.Sports] = TITLE_HEADER +                    "U.S. Pro Team Sports      [Ben LeMarc]",
            [EnumModels.HorseOrHuman] = TITLE_HEADER +              "Horse or Human?            [Official]",
            [EnumModels.Food101] = TITLE_HEADER +                   "Food-101                        [Official]",
            [EnumModels.HotdogNotHotdogFood101] = TITLE_HEADER +    "Hotdog or Not Hotdog?   [Food101]",

        };


        // The dictionary of model filenames. The highlighted model's filename will be passed to the classifier.
        // CAUTION:  Each filename must exist in the target or the program may crash.
        internal static IDictionary<EnumModels, string> ModelFilenames = new Dictionary<EnumModels, string>()
        {
            [EnumModels.HotdogNotHotdogFood101] = "MLModel.Hotdog_Not_Hotdog-Food101.Brian.gpu-200_epochs_no_stop.zip",
            [EnumModels.Vehicles] = "MLModel.Vehicles.Brian.gpu-default.no_early_stopping.zip",
            [EnumModels.CatsVsDogs] = "MLModel.Cats-vs-Dogs.gpu-50_epochs_null_stop.zip",
            [EnumModels.Mammals] = "MLModel.Mammals.Brian.gpu.29_epochs.no_early_stopping.zip",
            [EnumModels.Food101] = "MLModel.Food101.19_percent.Brian.zip",
            [EnumModels.StanfordDogs] = "MLModel.stanford_dogs.82_percent.brian.zip",
            [EnumModels.HorseOrHuman] = "MLModel.HorseOrHuman.Brian.v1.default-cpu.zip",
            [EnumModels.Sports] = "MLModel.Sports.Brian.gpu-default.zip",

        };


        // The dictionaries of prediction classes for each model.
        // CAUTION:  You must perfectly initialize this dictionary or the program may crash.  Use the MetaGenerator tool!
        internal static IDictionary<EnumModels, Dictionary<int, string>> PredictionClasses = new Dictionary<EnumModels, Dictionary<int, string>>()
        {

            [EnumModels.HotdogNotHotdogFood101] = new Dictionary<int, string>()
            {
                [0] = "hotdog",
                [1] = "not_hotdog"
            },


            [EnumModels.Food101] = new Dictionary<int, string>()
            {
                [0] = "apple_pie",
                [1] = "baby_back_ribs",
                [2] = "baklava",
                [3] = "beef_carpaccio",
                [4] = "beef_tartare",
                [5] = "beet_salad",
                [6] = "beignets",
                [7] = "bibimbap",
                [8] = "bread_pudding",
                [9] = "breakfast_burrito",
                [10] = "bruschetta",
                [11] = "caesar_salad",
                [12] = "cannoli",
                [13] = "caprese_salad",
                [14] = "carrot_cake",
                [15] = "ceviche",
                [16] = "cheesecake",
                [17] = "cheese_plate",
                [18] = "chicken_curry",
                [19] = "chicken_quesadilla",
                [20] = "chicken_wings",
                [21] = "chocolate_cake",
                [22] = "chocolate_mousse",
                [23] = "churros",
                [24] = "clam_chowder",
                [25] = "club_sandwich",
                [26] = "crab_cakes",
                [27] = "creme_brulee",
                [28] = "croque_madame",
                [29] = "cup_cakes",
                [30] = "deviled_eggs",
                [31] = "donuts",
                [32] = "dumplings",
                [33] = "edamame",
                [34] = "eggs_benedict",
                [35] = "escargots",
                [36] = "falafel",
                [37] = "filet_mignon",
                [38] = "fish_and_chips",
                [39] = "foie_gras",
                [40] = "french_fries",
                [41] = "french_onion_soup",
                [42] = "french_toast",
                [43] = "fried_calamari",
                [44] = "fried_rice",
                [45] = "frozen_yogurt",
                [46] = "garlic_bread",
                [47] = "gnocchi",
                [48] = "greek_salad",
                [49] = "grilled_cheese_sandwich",
                [50] = "grilled_salmon",
                [51] = "guacamole",
                [52] = "gyoza",
                [53] = "hamburger",
                [54] = "hot_and_sour_soup",
                [55] = "hot_dog",
                [56] = "huevos_rancheros",
                [57] = "hummus",
                [58] = "ice_cream",
                [59] = "lasagna",
                [60] = "lobster_bisque",
                [61] = "lobster_roll_sandwich",
                [62] = "macaroni_and_cheese",
                [63] = "macarons",
                [64] = "miso_soup",
                [65] = "mussels",
                [66] = "nachos",
                [67] = "omelette",
                [68] = "onion_rings",
                [69] = "oysters",
                [70] = "pad_thai",
                [71] = "paella",
                [72] = "pancakes",
                [73] = "panna_cotta",
                [74] = "peking_duck",
                [75] = "pho",
                [76] = "pizza",
                [77] = "pork_chop",
                [78] = "poutine",
                [79] = "prime_rib",
                [80] = "pulled_pork_sandwich",
                [81] = "ramen",
                [82] = "ravioli",
                [83] = "red_velvet_cake",
                [84] = "risotto",
                [85] = "samosa",
                [86] = "sashimi",
                [87] = "scallops",
                [88] = "seaweed_salad",
                [89] = "shrimp_and_grits",
                [90] = "spaghetti_bolognese",
                [91] = "spaghetti_carbonara",
                [92] = "spring_rolls",
                [93] = "steak",
                [94] = "strawberry_shortcake",
                [95] = "sushi",
                [96] = "tacos",
                [97] = "takoyaki",
                [98] = "tiramisu",
                [99] = "tuna_tartare",
                [100] = "waffles"
            },


            [EnumModels.StanfordDogs] = new Dictionary<int, string>()
            {
                [0] = "Chihuahua",
                [1] = "Japanese_spaniel",
                [2] = "Maltese_dog",
                [3] = "Pekinese",
                [4] = "Shih-Tzu",
                [5] = "Blenheim_spaniel",
                [6] = "papillon",
                [7] = "toy_terrier",
                [8] = "Rhodesian_ridgeback",
                [9] = "Afghan_hound",
                [10] = "basset",
                [11] = "beagle",
                [12] = "bloodhound",
                [13] = "bluetick",
                [14] = "black-and-tan_coonhound",
                [15] = "Walker_hound",
                [16] = "English_foxhound",
                [17] = "redbone",
                [18] = "borzoi",
                [19] = "Irish_wolfhound",
                [20] = "Italian_greyhound",
                [21] = "whippet",
                [22] = "Ibizan_hound",
                [23] = "Norwegian_elkhound",
                [24] = "otterhound",
                [25] = "Saluki",
                [26] = "Scottish_deerhound",
                [27] = "Weimaraner",
                [28] = "Staffordshire_bullterrier",
                [29] = "American_Staffordshire_terrier",
                [30] = "Bedlington_terrier",
                [31] = "Border_terrier",
                [32] = "Kerry_blue_terrier",
                [33] = "Irish_terrier",
                [34] = "Norfolk_terrier",
                [35] = "Norwich_terrier",
                [36] = "Yorkshire_terrier",
                [37] = "wire-haired_fox_terrier",
                [38] = "Lakeland_terrier",
                [39] = "Sealyham_terrier",
                [40] = "Airedale",
                [41] = "cairn",
                [42] = "Australian_terrier",
                [43] = "Dandie_Dinmont",
                [44] = "Boston_bull",
                [45] = "miniature_schnauzer",
                [46] = "giant_schnauzer",
                [47] = "standard_schnauzer",
                [48] = "Scotch_terrier",
                [49] = "Tibetan_terrier",
                [50] = "silky_terrier",
                [51] = "soft-coated_wheaten_terrier",
                [52] = "West_Highland_white_terrier",
                [53] = "Lhasa",
                [54] = "flat-coated_retriever",
                [55] = "curly-coated_retriever",
                [56] = "golden_retriever",
                [57] = "Labrador_retriever",
                [58] = "Chesapeake_Bay_retriever",
                [59] = "German_short-haired_pointer",
                [60] = "vizsla",
                [61] = "English_setter",
                [62] = "Irish_setter",
                [63] = "Gordon_setter",
                [64] = "Brittany_spaniel",
                [65] = "clumber",
                [66] = "English_springer",
                [67] = "Welsh_springer_spaniel",
                [68] = "cocker_spaniel",
                [69] = "Sussex_spaniel",
                [70] = "Irish_water_spaniel",
                [71] = "kuvasz",
                [72] = "schipperke",
                [73] = "groenendael",
                [74] = "malinois",
                [75] = "briard",
                [76] = "kelpie",
                [77] = "komondor",
                [78] = "Old_English_sheepdog",
                [79] = "Shetland_sheepdog",
                [80] = "collie",
                [81] = "Border_collie",
                [82] = "Bouvier_des_Flandres",
                [83] = "Rottweiler",
                [84] = "German_shepherd",
                [85] = "Doberman",
                [86] = "miniature_pinscher",
                [87] = "Greater_Swiss_Mountain_dog",
                [88] = "Bernese_mountain_dog",
                [89] = "Appenzeller",
                [90] = "EntleBucher",
                [91] = "boxer",
                [92] = "bull_mastiff",
                [93] = "Tibetan_mastiff",
                [94] = "French_bulldog",
                [95] = "Great_Dane",
                [96] = "Saint_Bernard",
                [97] = "Eskimo_dog",
                [98] = "malamute",
                [99] = "Siberian_husky",
                [100] = "affenpinscher",
                [101] = "basenji",
                [102] = "pug",
                [103] = "Leonberg",
                [104] = "Newfoundland",
                [105] = "Great_Pyrenees",
                [106] = "Samoyed",
                [107] = "Pomeranian",
                [108] = "chow",
                [109] = "keeshond",
                [110] = "Brabancon_griffon",
                [111] = "Pembroke",
                [112] = "Cardigan",
                [113] = "toy_poodle",
                [114] = "miniature_poodle",
                [115] = "standard_poodle",
                [116] = "Mexican_hairless",
                [117] = "dingo",
                [118] = "dhole",
                [119] = "African_hunting_dog",
            },


            [EnumModels.Sports] = new Dictionary<int, string>()
            {
                [0] = "Baseball",
                [1] = "Basketball",
                [2] = "Football",
                [3] = "Hockey",
                [4] = "Soccer",
            },


            [EnumModels.Vehicles] = new Dictionary<int, string>()
            {
                [0] = "Bicycle",
                [1] = "Bus",
                [2] = "Convertible",
                [3] = "Freight_Truck",
                [4] = "Golf_Cart",
                [5] = "Hearse",
                [6] = "Limousine",
                [7] = "Model_T",
                [8] = "Monster_Truck",
                [9] = "Motorcycle",
                [10] = "Pickup_Truck",
                [11] = "Police_Car",
                [12] = "Racecar",
                [13] = "RV",
                [14] = "Sedan",
                [15] = "SUV",
                [16] = "Van",
            },

            
            [EnumModels.Mammals] = new Dictionary<int, string>()
            {
                [0] = "Bear",
                [1] = "Beaver",
                [2] = "Cat",
                [3] = "Chipmunk",
                [4] = "Cougar",
                [5] = "Cow",
                [6] = "Coyote",
                [7] = "Deer",
                [8] = "Dog",
                [9] = "Elephant",
                [10] = "Fox",
                [11] = "Giraffe",
                [12] = "Goat",
                [13] = "Horse",
                [14] = "Human",
                [15] = "Lion",
                [16] = "Lynx",
                [17] = "Meerkat",
                [18] = "Moose",
                [19] = "Muskrat",
                [20] = "Opossum",
                [21] = "Otter",
                [22] = "Panda",
                [23] = "Pig",
                [24] = "Rabbit",
                [25] = "Racoon",
                [26] = "Sheep",
                [27] = "Skunk",
                [28] = "Sloth",
                [29] = "Squirrel",
                [30] = "Tiger",
                [31] = "Wolf",
            },


            [EnumModels.CatsVsDogs] = new Dictionary<int, string>()
            {
                [0] = "cat",
                [1] = "dog",
            },


            [EnumModels.HorseOrHuman] = new Dictionary<int, string>()
            {
                [0] = "horses",
                [1] = "humans",
            },



        };

    }
}
